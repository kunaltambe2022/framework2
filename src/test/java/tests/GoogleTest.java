package tests;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import config.BaseClass;
import config.ReadProperty;
import utility.NoSuchDriverException;

public class GoogleTest {

	WebDriver driver;

	@Test
	public void googleTest() throws FileNotFoundException, IOException, NoSuchDriverException {
		ReadProperty.setProperty("browser", "chrome");
		driver = BaseClass.getDriver(ReadProperty.getProperty("browser"));
		driver.navigate().to("https://www.google.co.in");

		System.out.println(driver.getTitle());
		driver.close();
	}

}
