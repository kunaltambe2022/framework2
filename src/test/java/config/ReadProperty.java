package config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Optional;
import java.util.Properties;

public class ReadProperty {
	static String configFilePath = System.getProperty("user.dir") + "//config.properties";
	static File configFile;
	static Properties properties;

	public static String getProperty(String property) throws FileNotFoundException, IOException {
		configFile = new File(configFilePath);
		properties = new Properties();
		properties.load(new FileInputStream(configFile));

		if (properties.get(property) == null) {
			return "Invalid Property";
		}
		return properties.get(property).toString();
	}

	public static void setProperty(String key, String value) throws FileNotFoundException, IOException {

		configFile = new File(configFilePath);
		properties = new Properties();
		properties.setProperty(key, value);
		FileOutputStream fos = new FileOutputStream(configFile);
		properties.store(fos, null);

		fos.close();

	}

	public static void main(String[] args) throws FileNotFoundException, IOException {
		ReadProperty.setProperty("first", "firstaaaa");
		System.out.println(ReadProperty.getProperty("first"));

	}
}
