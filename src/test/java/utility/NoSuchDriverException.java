package utility;

public class NoSuchDriverException extends Exception {

	public NoSuchDriverException(String Message) {
		super(Message);
	}

}
